<%--
  Created by IntelliJ IDEA.
  User: Anna
  Date: 01.09.2019
  Time: 10:21
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h2>Hello ${sessionScope.user.userName}</h2>
<h2>Your role is ${sessionScope.user.role}</h2>
<table style="width:100%; border: 1px">
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Age</th>
        <th>Phone number</th>
    </tr>
    <c:forEach var="person" items="${requestScope.personList}">
        <form action="/persons" method="post">
            <tr>
                <td style="display: none">
                        <%--                <label for="person_id">${person.id}</label>--%>
                    <input type="hidden" id="person_id" name="person_id" value="${person.id}"/>
                </td>
                <td>${person.firstName}</td>
                <td>${person.lastName}</td>
                <td>${person.age}</td>
                <td>${person.phoneNumber}</td>
                <td>
                    <c:if test="${sessionScope.user.role == 'ADMIN'}">
                        <input type="submit" name="operationMode" value="EDIT">
                        <input type="submit" name="operationMode" value="DELETE">
                    </c:if>
                </td>
            </tr>
        </form>
    </c:forEach>
</table>
<c:if test="${sessionScope.user.role == 'ADMIN'}">
<form action="/persons/add" method="get">
    <input type="submit" value="Dodaj nową osobę">
</form>
</c:if>

<%--<c:if test="${cookie.permission-for-cookies.value}">--%>
    <jsp:include page="cookie.jsp"></jsp:include>
<%--</c:if>--%>

<ul>
    <c:forEach var="cookies" items="${cookie}">
        <li>
            <c:out value="${cookies.key}"/>:
            Object=<c:out value="${cookies.value}"/>,
            value=<c:out value="${cookies.value.value}"/>
        </li>
    </c:forEach>
</ul>
</body>
</html>
